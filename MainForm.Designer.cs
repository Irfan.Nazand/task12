﻿namespace CharacterForms
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblName = new System.Windows.Forms.Label();
            this.TxtName = new System.Windows.Forms.TextBox();
            this.CboxType = new System.Windows.Forms.ComboBox();
            this.LblType = new System.Windows.Forms.Label();
            this.BtnCreate = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnSave = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbl9 = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl12 = new System.Windows.Forms.Label();
            this.lbl11 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lbl14 = new System.Windows.Forms.Label();
            this.lbl15 = new System.Windows.Forms.Label();
            this.lbl43 = new System.Windows.Forms.Label();
            this.lbl37 = new System.Windows.Forms.Label();
            this.lbl31 = new System.Windows.Forms.Label();
            this.lbl25 = new System.Windows.Forms.Label();
            this.lbl19 = new System.Windows.Forms.Label();
            this.lbl13 = new System.Windows.Forms.Label();
            this.lbl20 = new System.Windows.Forms.Label();
            this.lbl39 = new System.Windows.Forms.Label();
            this.lbl45 = new System.Windows.Forms.Label();
            this.lbl32 = new System.Windows.Forms.Label();
            this.lbl38 = new System.Windows.Forms.Label();
            this.lbl44 = new System.Windows.Forms.Label();
            this.lbl18 = new System.Windows.Forms.Label();
            this.lbl17 = new System.Windows.Forms.Label();
            this.lbl16 = new System.Windows.Forms.Label();
            this.lbl46 = new System.Windows.Forms.Label();
            this.lbl36 = new System.Windows.Forms.Label();
            this.lbl22 = new System.Windows.Forms.Label();
            this.lbl35 = new System.Windows.Forms.Label();
            this.lbl34 = new System.Windows.Forms.Label();
            this.lbl33 = new System.Windows.Forms.Label();
            this.lbl23 = new System.Windows.Forms.Label();
            this.lbl24 = new System.Windows.Forms.Label();
            this.lbl27 = new System.Windows.Forms.Label();
            this.lbl28 = new System.Windows.Forms.Label();
            this.lbl29 = new System.Windows.Forms.Label();
            this.lbl30 = new System.Windows.Forms.Label();
            this.lbl21 = new System.Windows.Forms.Label();
            this.lbl48 = new System.Windows.Forms.Label();
            this.lbl42 = new System.Windows.Forms.Label();
            this.lbl47 = new System.Windows.Forms.Label();
            this.lbl41 = new System.Windows.Forms.Label();
            this.lbl40 = new System.Windows.Forms.Label();
            this.lbl26 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LblName
            // 
            this.LblName.AutoSize = true;
            this.LblName.Location = new System.Drawing.Point(12, 15);
            this.LblName.Name = "LblName";
            this.LblName.Size = new System.Drawing.Size(138, 25);
            this.LblName.TabIndex = 0;
            this.LblName.Text = "Character Name";
            this.LblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtName
            // 
            this.TxtName.Location = new System.Drawing.Point(156, 15);
            this.TxtName.Name = "TxtName";
            this.TxtName.Size = new System.Drawing.Size(237, 31);
            this.TxtName.TabIndex = 1;
            this.TxtName.TextChanged += new System.EventHandler(this.TxtName_TextChanged);
            // 
            // CboxType
            // 
            this.CboxType.FormattingEnabled = true;
            this.CboxType.Items.AddRange(new object[] {
            "Wizard",
            "Dwarf",
            "Thief",
            "Warrior"});
            this.CboxType.Location = new System.Drawing.Point(156, 54);
            this.CboxType.Name = "CboxType";
            this.CboxType.Size = new System.Drawing.Size(237, 33);
            this.CboxType.TabIndex = 2;
            this.CboxType.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // LblType
            // 
            this.LblType.AutoSize = true;
            this.LblType.Location = new System.Drawing.Point(12, 57);
            this.LblType.Name = "LblType";
            this.LblType.Size = new System.Drawing.Size(128, 25);
            this.LblType.TabIndex = 3;
            this.LblType.Text = "Character Type";
            this.LblType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnCreate
            // 
            this.BtnCreate.Location = new System.Drawing.Point(177, 93);
            this.BtnCreate.Name = "BtnCreate";
            this.BtnCreate.Size = new System.Drawing.Size(154, 41);
            this.BtnCreate.TabIndex = 4;
            this.BtnCreate.Text = "Create Character";
            this.BtnCreate.UseVisualStyleBackColor = true;
            this.BtnCreate.Click += new System.EventHandler(this.BtnCreateCharacter_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 25;
            this.listBox1.Location = new System.Drawing.Point(156, 190);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(202, 204);
            this.listBox1.TabIndex = 5;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 190);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(97, 215);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Type:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(84, 240);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "Ability:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(111, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 25);
            this.label4.TabIndex = 9;
            this.label4.Text = "HP:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(80, 290);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 25);
            this.label5.TabIndex = 10;
            this.label5.Text = "Energy:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 315);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 25);
            this.label6.TabIndex = 11;
            this.label6.Text = "Armor Rating:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(156, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(191, 32);
            this.label7.TabIndex = 12;
            this.label7.Text = "Your Character:";
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(177, 402);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(154, 34);
            this.BtnSave.TabIndex = 13;
            this.BtnSave.Text = "Save Character";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(527, 470);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(232, 34);
            this.button1.TabIndex = 14;
            this.button1.Text = "Show Saved Characters";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 25;
            this.listBox2.Location = new System.Drawing.Point(410, 70);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(843, 379);
            this.listBox2.TabIndex = 15;
            this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(437, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 25);
            this.label8.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(571, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(128, 30);
            this.label10.TabIndex = 18;
            this.label10.Text = "Name";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(437, 93);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 30);
            this.label11.TabIndex = 18;
            this.label11.Text = "Type";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(1107, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 30);
            this.label9.TabIndex = 18;
            this.label9.Text = "Armor";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(973, 93);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 30);
            this.label13.TabIndex = 18;
            this.label13.Text = "Energy";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(839, 93);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(128, 30);
            this.label14.TabIndex = 18;
            this.label14.Text = "HP";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(705, 93);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(128, 30);
            this.label15.TabIndex = 18;
            this.label15.Text = "Ability";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl9
            // 
            this.lbl9.Location = new System.Drawing.Point(705, 158);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(128, 30);
            this.lbl9.TabIndex = 18;
            this.lbl9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl8
            // 
            this.lbl8.Location = new System.Drawing.Point(571, 158);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(128, 30);
            this.lbl8.TabIndex = 18;
            this.lbl8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl7
            // 
            this.lbl7.Location = new System.Drawing.Point(437, 158);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(128, 30);
            this.lbl7.TabIndex = 18;
            this.lbl7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl6
            // 
            this.lbl6.Location = new System.Drawing.Point(1107, 126);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(128, 30);
            this.lbl6.TabIndex = 18;
            this.lbl6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl5
            // 
            this.lbl5.Location = new System.Drawing.Point(973, 126);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(128, 30);
            this.lbl5.TabIndex = 18;
            this.lbl5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl4
            // 
            this.lbl4.Location = new System.Drawing.Point(839, 126);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(128, 30);
            this.lbl4.TabIndex = 18;
            this.lbl4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl4.Click += new System.EventHandler(this.label20_Click);
            // 
            // lbl3
            // 
            this.lbl3.Location = new System.Drawing.Point(705, 126);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(128, 30);
            this.lbl3.TabIndex = 18;
            this.lbl3.Text = "\r\n";
            this.lbl3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl2
            // 
            this.lbl2.Location = new System.Drawing.Point(571, 126);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(128, 30);
            this.lbl2.TabIndex = 18;
            this.lbl2.Text = "\r\n";
            this.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl1
            // 
            this.lbl1.Location = new System.Drawing.Point(437, 126);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(128, 30);
            this.lbl1.TabIndex = 18;
            this.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl12
            // 
            this.lbl12.Location = new System.Drawing.Point(1107, 158);
            this.lbl12.Name = "lbl12";
            this.lbl12.Size = new System.Drawing.Size(128, 30);
            this.lbl12.TabIndex = 18;
            this.lbl12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl11
            // 
            this.lbl11.Location = new System.Drawing.Point(973, 158);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(128, 30);
            this.lbl11.TabIndex = 18;
            this.lbl11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl10
            // 
            this.lbl10.Location = new System.Drawing.Point(839, 158);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(128, 30);
            this.lbl10.TabIndex = 18;
            this.lbl10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl14
            // 
            this.lbl14.Location = new System.Drawing.Point(571, 199);
            this.lbl14.Name = "lbl14";
            this.lbl14.Size = new System.Drawing.Size(128, 30);
            this.lbl14.TabIndex = 18;
            this.lbl14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl15
            // 
            this.lbl15.Location = new System.Drawing.Point(705, 199);
            this.lbl15.Name = "lbl15";
            this.lbl15.Size = new System.Drawing.Size(128, 30);
            this.lbl15.TabIndex = 18;
            this.lbl15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl43
            // 
            this.lbl43.Location = new System.Drawing.Point(437, 402);
            this.lbl43.Name = "lbl43";
            this.lbl43.Size = new System.Drawing.Size(128, 30);
            this.lbl43.TabIndex = 18;
            this.lbl43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl37
            // 
            this.lbl37.Location = new System.Drawing.Point(437, 364);
            this.lbl37.Name = "lbl37";
            this.lbl37.Size = new System.Drawing.Size(128, 30);
            this.lbl37.TabIndex = 18;
            this.lbl37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl31
            // 
            this.lbl31.Location = new System.Drawing.Point(437, 312);
            this.lbl31.Name = "lbl31";
            this.lbl31.Size = new System.Drawing.Size(128, 30);
            this.lbl31.TabIndex = 18;
            this.lbl31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl25
            // 
            this.lbl25.Location = new System.Drawing.Point(437, 280);
            this.lbl25.Name = "lbl25";
            this.lbl25.Size = new System.Drawing.Size(128, 30);
            this.lbl25.TabIndex = 18;
            this.lbl25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl19
            // 
            this.lbl19.Location = new System.Drawing.Point(437, 231);
            this.lbl19.Name = "lbl19";
            this.lbl19.Size = new System.Drawing.Size(128, 30);
            this.lbl19.TabIndex = 18;
            this.lbl19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl13
            // 
            this.lbl13.Location = new System.Drawing.Point(437, 199);
            this.lbl13.Name = "lbl13";
            this.lbl13.Size = new System.Drawing.Size(128, 30);
            this.lbl13.TabIndex = 18;
            this.lbl13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl20
            // 
            this.lbl20.Location = new System.Drawing.Point(571, 231);
            this.lbl20.Name = "lbl20";
            this.lbl20.Size = new System.Drawing.Size(128, 30);
            this.lbl20.TabIndex = 18;
            this.lbl20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl39
            // 
            this.lbl39.Location = new System.Drawing.Point(705, 364);
            this.lbl39.Name = "lbl39";
            this.lbl39.Size = new System.Drawing.Size(128, 30);
            this.lbl39.TabIndex = 18;
            this.lbl39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl45
            // 
            this.lbl45.Location = new System.Drawing.Point(705, 404);
            this.lbl45.Name = "lbl45";
            this.lbl45.Size = new System.Drawing.Size(128, 30);
            this.lbl45.TabIndex = 18;
            this.lbl45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl32
            // 
            this.lbl32.Location = new System.Drawing.Point(571, 312);
            this.lbl32.Name = "lbl32";
            this.lbl32.Size = new System.Drawing.Size(128, 30);
            this.lbl32.TabIndex = 18;
            this.lbl32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl38
            // 
            this.lbl38.Location = new System.Drawing.Point(571, 364);
            this.lbl38.Name = "lbl38";
            this.lbl38.Size = new System.Drawing.Size(128, 30);
            this.lbl38.TabIndex = 18;
            this.lbl38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl44
            // 
            this.lbl44.Location = new System.Drawing.Point(571, 404);
            this.lbl44.Name = "lbl44";
            this.lbl44.Size = new System.Drawing.Size(128, 30);
            this.lbl44.TabIndex = 18;
            this.lbl44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl18
            // 
            this.lbl18.Location = new System.Drawing.Point(1107, 199);
            this.lbl18.Name = "lbl18";
            this.lbl18.Size = new System.Drawing.Size(128, 30);
            this.lbl18.TabIndex = 18;
            this.lbl18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl17
            // 
            this.lbl17.Location = new System.Drawing.Point(973, 199);
            this.lbl17.Name = "lbl17";
            this.lbl17.Size = new System.Drawing.Size(128, 30);
            this.lbl17.TabIndex = 18;
            this.lbl17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl16
            // 
            this.lbl16.Location = new System.Drawing.Point(839, 199);
            this.lbl16.Name = "lbl16";
            this.lbl16.Size = new System.Drawing.Size(128, 30);
            this.lbl16.TabIndex = 18;
            this.lbl16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl46
            // 
            this.lbl46.Location = new System.Drawing.Point(839, 404);
            this.lbl46.Name = "lbl46";
            this.lbl46.Size = new System.Drawing.Size(128, 30);
            this.lbl46.TabIndex = 18;
            this.lbl46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl36
            // 
            this.lbl36.Location = new System.Drawing.Point(1107, 312);
            this.lbl36.Name = "lbl36";
            this.lbl36.Size = new System.Drawing.Size(128, 30);
            this.lbl36.TabIndex = 18;
            this.lbl36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl22
            // 
            this.lbl22.Location = new System.Drawing.Point(839, 231);
            this.lbl22.Name = "lbl22";
            this.lbl22.Size = new System.Drawing.Size(128, 30);
            this.lbl22.TabIndex = 18;
            this.lbl22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl35
            // 
            this.lbl35.Location = new System.Drawing.Point(973, 312);
            this.lbl35.Name = "lbl35";
            this.lbl35.Size = new System.Drawing.Size(128, 30);
            this.lbl35.TabIndex = 18;
            this.lbl35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl34
            // 
            this.lbl34.Location = new System.Drawing.Point(839, 312);
            this.lbl34.Name = "lbl34";
            this.lbl34.Size = new System.Drawing.Size(128, 30);
            this.lbl34.TabIndex = 18;
            this.lbl34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl33
            // 
            this.lbl33.Location = new System.Drawing.Point(705, 312);
            this.lbl33.Name = "lbl33";
            this.lbl33.Size = new System.Drawing.Size(128, 30);
            this.lbl33.TabIndex = 18;
            this.lbl33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl23
            // 
            this.lbl23.Location = new System.Drawing.Point(973, 231);
            this.lbl23.Name = "lbl23";
            this.lbl23.Size = new System.Drawing.Size(128, 30);
            this.lbl23.TabIndex = 18;
            this.lbl23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl24
            // 
            this.lbl24.Location = new System.Drawing.Point(1107, 231);
            this.lbl24.Name = "lbl24";
            this.lbl24.Size = new System.Drawing.Size(128, 30);
            this.lbl24.TabIndex = 18;
            this.lbl24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl27
            // 
            this.lbl27.Location = new System.Drawing.Point(705, 280);
            this.lbl27.Name = "lbl27";
            this.lbl27.Size = new System.Drawing.Size(128, 30);
            this.lbl27.TabIndex = 18;
            this.lbl27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl28
            // 
            this.lbl28.Location = new System.Drawing.Point(839, 280);
            this.lbl28.Name = "lbl28";
            this.lbl28.Size = new System.Drawing.Size(128, 30);
            this.lbl28.TabIndex = 18;
            this.lbl28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl29
            // 
            this.lbl29.Location = new System.Drawing.Point(973, 280);
            this.lbl29.Name = "lbl29";
            this.lbl29.Size = new System.Drawing.Size(128, 30);
            this.lbl29.TabIndex = 18;
            this.lbl29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl30
            // 
            this.lbl30.Location = new System.Drawing.Point(1107, 280);
            this.lbl30.Name = "lbl30";
            this.lbl30.Size = new System.Drawing.Size(128, 30);
            this.lbl30.TabIndex = 18;
            this.lbl30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl21
            // 
            this.lbl21.Location = new System.Drawing.Point(705, 231);
            this.lbl21.Name = "lbl21";
            this.lbl21.Size = new System.Drawing.Size(128, 30);
            this.lbl21.TabIndex = 18;
            this.lbl21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl48
            // 
            this.lbl48.Location = new System.Drawing.Point(1107, 406);
            this.lbl48.Name = "lbl48";
            this.lbl48.Size = new System.Drawing.Size(128, 30);
            this.lbl48.TabIndex = 18;
            this.lbl48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl42
            // 
            this.lbl42.Location = new System.Drawing.Point(1107, 364);
            this.lbl42.Name = "lbl42";
            this.lbl42.Size = new System.Drawing.Size(128, 30);
            this.lbl42.TabIndex = 18;
            this.lbl42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl47
            // 
            this.lbl47.Location = new System.Drawing.Point(973, 406);
            this.lbl47.Name = "lbl47";
            this.lbl47.Size = new System.Drawing.Size(128, 30);
            this.lbl47.TabIndex = 18;
            this.lbl47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl41
            // 
            this.lbl41.Location = new System.Drawing.Point(973, 364);
            this.lbl41.Name = "lbl41";
            this.lbl41.Size = new System.Drawing.Size(128, 30);
            this.lbl41.TabIndex = 18;
            this.lbl41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl40
            // 
            this.lbl40.Location = new System.Drawing.Point(839, 364);
            this.lbl40.Name = "lbl40";
            this.lbl40.Size = new System.Drawing.Size(128, 30);
            this.lbl40.TabIndex = 18;
            this.lbl40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl26
            // 
            this.lbl26.Location = new System.Drawing.Point(571, 280);
            this.lbl26.Name = "lbl26";
            this.lbl26.Size = new System.Drawing.Size(128, 30);
            this.lbl26.TabIndex = 18;
            this.lbl26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1265, 526);
            this.Controls.Add(this.lbl26);
            this.Controls.Add(this.lbl40);
            this.Controls.Add(this.lbl41);
            this.Controls.Add(this.lbl47);
            this.Controls.Add(this.lbl42);
            this.Controls.Add(this.lbl48);
            this.Controls.Add(this.lbl21);
            this.Controls.Add(this.lbl30);
            this.Controls.Add(this.lbl29);
            this.Controls.Add(this.lbl28);
            this.Controls.Add(this.lbl27);
            this.Controls.Add(this.lbl24);
            this.Controls.Add(this.lbl23);
            this.Controls.Add(this.lbl33);
            this.Controls.Add(this.lbl34);
            this.Controls.Add(this.lbl35);
            this.Controls.Add(this.lbl22);
            this.Controls.Add(this.lbl36);
            this.Controls.Add(this.lbl46);
            this.Controls.Add(this.lbl16);
            this.Controls.Add(this.lbl17);
            this.Controls.Add(this.lbl18);
            this.Controls.Add(this.lbl44);
            this.Controls.Add(this.lbl38);
            this.Controls.Add(this.lbl32);
            this.Controls.Add(this.lbl45);
            this.Controls.Add(this.lbl39);
            this.Controls.Add(this.lbl20);
            this.Controls.Add(this.lbl13);
            this.Controls.Add(this.lbl19);
            this.Controls.Add(this.lbl25);
            this.Controls.Add(this.lbl31);
            this.Controls.Add(this.lbl37);
            this.Controls.Add(this.lbl43);
            this.Controls.Add(this.lbl15);
            this.Controls.Add(this.lbl14);
            this.Controls.Add(this.lbl10);
            this.Controls.Add(this.lbl11);
            this.Controls.Add(this.lbl12);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lbl6);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.lbl8);
            this.Controls.Add(this.lbl9);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.BtnCreate);
            this.Controls.Add(this.LblType);
            this.Controls.Add(this.CboxType);
            this.Controls.Add(this.TxtName);
            this.Controls.Add(this.LblName);
            this.Name = "MainForm";
            this.Text = "Choose your character";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblName;
        private System.Windows.Forms.TextBox TxtName;
        private System.Windows.Forms.ComboBox CboxType;
        private System.Windows.Forms.Label LblType;
        private System.Windows.Forms.Button BtnCreateCharacter;
        private System.Windows.Forms.Button BtnCreate;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl12;
        private System.Windows.Forms.Label lbl11;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lbl14;
        private System.Windows.Forms.Label lbl15;
        private System.Windows.Forms.Label lbl43;
        private System.Windows.Forms.Label lbl37;
        private System.Windows.Forms.Label lbl31;
        private System.Windows.Forms.Label lbl25;
        private System.Windows.Forms.Label lbl19;
        private System.Windows.Forms.Label lbl13;
        private System.Windows.Forms.Label lbl20;
        private System.Windows.Forms.Label lbl39;
        private System.Windows.Forms.Label lbl45;
        private System.Windows.Forms.Label lbl32;
        private System.Windows.Forms.Label lbl38;
        private System.Windows.Forms.Label lbl44;
        private System.Windows.Forms.Label lbl18;
        private System.Windows.Forms.Label lbl17;
        private System.Windows.Forms.Label lbl16;
        private System.Windows.Forms.Label lbl46;
        private System.Windows.Forms.Label lbl36;
        private System.Windows.Forms.Label lbl22;
        private System.Windows.Forms.Label lbl35;
        private System.Windows.Forms.Label lbl34;
        private System.Windows.Forms.Label lbl33;
        private System.Windows.Forms.Label lbl23;
        private System.Windows.Forms.Label lbl24;
        private System.Windows.Forms.Label lbl27;
        private System.Windows.Forms.Label lbl28;
        private System.Windows.Forms.Label lbl29;
        private System.Windows.Forms.Label lbl30;
        private System.Windows.Forms.Label lbl21;
        private System.Windows.Forms.Label lbl48;
        private System.Windows.Forms.Label lbl42;
        private System.Windows.Forms.Label lbl47;
        private System.Windows.Forms.Label lbl41;
        private System.Windows.Forms.Label lbl40;
        private System.Windows.Forms.Label lbl26;
    }
}

