﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPGCharacterGenerator.Characters;
using RPGCharacterGenerator;
using System.Data.SqlClient;
using System.Net.Http.Headers;

namespace CharacterForms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void TxtName_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        // On click handler create button, creates the character after double clicking the create button 
        private void BtnCreateCharacter_Click(object sender, EventArgs e)
        {

            string type = CboxType.Text;
            string navn = TxtName.Text;

            Wizard wiz = new Wizard(type, navn, "Spells", 100, 150, 10);
            Dwarf dwrf = new Dwarf(type, navn, "Axe Swing", 150, 80, 200);
            Warrior warrior = new Warrior(type, navn, "Warrior RAGE!!", 100, 200, 50);
            Thief thief = new Thief(type, navn, "Sneak", 100, 250, 10);

            if (String.IsNullOrEmpty(TxtName.Text) || TxtName.Text[0] == ' ')
            {
                MessageBox.Show("Please enter character name.");
                return;
            }
            else if (this.CboxType.SelectedItem == null)
            {
                MessageBox.Show("Please select a character type.");
                return;
            }
            else 
            {
                if (type == "Wizard")
                {
                    listBox1.Items.Add(wiz.Type);
                    listBox1.Items.Add(wiz.Spell);
                    listBox1.Items.Add(wiz.Name);
                    listBox1.Items.Add(wiz.Hp);
                    listBox1.Items.Add(wiz.Energy);
                    listBox1.Items.Add(wiz.ArmorRating);
                }
                else if (type == "Dwarf")
                {
                    listBox1.Items.Add(dwrf.Type);
                    listBox1.Items.Add(dwrf.AxeSwing);
                    listBox1.Items.Add(dwrf.Name);
                    listBox1.Items.Add(dwrf.Hp);
                    listBox1.Items.Add(dwrf.Energy);
                    listBox1.Items.Add(dwrf.ArmorRating);
                }
                else if (type == "Warrior")
                {
                    listBox1.Items.Add(warrior.Type);
                    listBox1.Items.Add(warrior.WarriorRage);
                    listBox1.Items.Add(warrior.Name);
                    listBox1.Items.Add(warrior.Hp);
                    listBox1.Items.Add(warrior.Energy);
                    listBox1.Items.Add(warrior.ArmorRating);
                }
                else if (type == "Thief")
                {
                    listBox1.Items.Add(thief.Type);
                    listBox1.Items.Add(thief.Sneak);
                    listBox1.Items.Add(thief.Name);
                    listBox1.Items.Add(thief.Hp);
                    listBox1.Items.Add(thief.Energy);
                    listBox1.Items.Add(thief.ArmorRating);
                }
            }
        }

        //Saves the created Character in the Database
        private void BtnSave_Click(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7269\\SQLEXPRESS01";
            builder.InitialCatalog = "CharacterDB";
            builder.IntegratedSecurity = true;

            string CharacterType = listBox1.Items[0].ToString();
            string Name = "";
            string Ability = "";
            string HP = "";
            string Energy = "";
            string ArmorRating = "";

            string sql = $"INSERT INTO CharacterTable (CharacterType, Name, Ability, HP, Energy, ArmorRating) VALUES (@characterType, @name, @ability, @HP, @Energy, @ArmorRating)";

            switch (CharacterType)
            {
                case ("Wizard"):
                    Name = TxtName.Text;
                    Ability = (string)listBox1.Items[2];
                    HP = listBox1.Items[3].ToString();
                    Energy = listBox1.Items[4].ToString();
                    ArmorRating = listBox1.Items[5].ToString();

                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();
                        Console.WriteLine("Done. \n");

                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@characterType", CharacterType);
                            command.Parameters.AddWithValue("@name", Name);
                            command.Parameters.AddWithValue("@ability", Ability);
                            command.Parameters.AddWithValue("@HP", HP);
                            command.Parameters.AddWithValue("@Energy", Energy);
                            command.Parameters.AddWithValue("@ArmorRating", ArmorRating);

                            command.ExecuteNonQuery();
                            listBox1.Items.Clear();
                        }
                    }
                    break;

                case ("Dwarf"):
                    Name = TxtName.Text;
                    Ability = (string)listBox1.Items[2];
                    HP = listBox1.Items[3].ToString();
                    Energy = listBox1.Items[4].ToString();
                    ArmorRating = listBox1.Items[5].ToString();

                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();

                        Console.WriteLine("Done. \n");

                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@characterType", CharacterType);
                            command.Parameters.AddWithValue("@name", Name);
                            command.Parameters.AddWithValue("@ability", Ability);
                            command.Parameters.AddWithValue("@HP", HP);
                            command.Parameters.AddWithValue("@Energy", Energy);
                            command.Parameters.AddWithValue("@ArmorRating", ArmorRating);

                            command.ExecuteNonQuery();
                            listBox1.Items.Clear();
                        }
                    }
                    break;

                case ("Thief"):
                    Name = TxtName.Text;
                    Ability = (string)listBox1.Items[2];
                    HP = listBox1.Items[3].ToString();
                    Energy = listBox1.Items[4].ToString();
                    ArmorRating = listBox1.Items[5].ToString();

                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();

                        Console.WriteLine("Done. \n");

                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@characterType", CharacterType);
                            command.Parameters.AddWithValue("@name", Name);
                            command.Parameters.AddWithValue("@ability", Ability);
                            command.Parameters.AddWithValue("@HP", HP);
                            command.Parameters.AddWithValue("@Energy", Energy);
                            command.Parameters.AddWithValue("@ArmorRating", ArmorRating);

                            command.ExecuteNonQuery();
                            listBox1.Items.Clear();
                        }
                    }
                    break;

                case ("Warrior"):
                    Name = TxtName.Text;
                    Ability = (string)listBox1.Items[2];
                    HP = listBox1.Items[3].ToString();
                    Energy = listBox1.Items[4].ToString();
                    ArmorRating = listBox1.Items[5].ToString();

                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();

                        Console.WriteLine("Done. \n");

                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@characterType", CharacterType);
                            command.Parameters.AddWithValue("@name", Name);
                            command.Parameters.AddWithValue("@ability", Ability);
                            command.Parameters.AddWithValue("@HP", HP);
                            command.Parameters.AddWithValue("@Energy", Energy);
                            command.Parameters.AddWithValue("@ArmorRating", ArmorRating);

                            command.ExecuteNonQuery();
                            listBox1.Items.Clear();
                        }
                    }
                    break;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7269\\SQLEXPRESS01";
            builder.InitialCatalog = "CharacterDB";
            builder.IntegratedSecurity = true;

            try
            {
                // Connecting to the database
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    //List values from the database
                    string Wizard = "SELECT * FROM CharacterTable";

                    using (SqlCommand command = new SqlCommand(Wizard, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            reader.Read();
                            lbl1.Text = reader.GetString(1);
                            lbl2.Text = reader.GetString(2);
                            lbl3.Text = reader.GetString(3);
                            lbl4.Text = reader.GetString(4);
                            lbl5.Text = reader.GetString(5);
                            lbl6.Text = reader.GetString(6);

                            reader.Read();
                            lbl7.Text = reader.GetString(1);
                            lbl8.Text = reader.GetString(2);
                            lbl9.Text = reader.GetString(3);
                            lbl10.Text = reader.GetString(4);
                            lbl11.Text = reader.GetString(5);
                            lbl12.Text = reader.GetString(6);

                        }
                    }

                    string Dwarf = "SELECT * FROM CharacterTable";

                    using (SqlCommand command = new SqlCommand(Dwarf, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            reader.Read();
                            lbl13.Text = reader.GetString(1);
                            lbl14.Text = reader.GetString(2);
                            lbl15.Text = reader.GetString(3);
                            lbl16.Text = reader.GetString(4);
                            lbl7.Text = reader.GetString(5);
                            lbl8.Text = reader.GetString(6);

                            reader.Read();
                            lbl19.Text = reader.GetString(1);
                            lbl20.Text = reader.GetString(2);
                            lbl21.Text = reader.GetString(3);
                            lbl22.Text = reader.GetString(4);
                            lbl23.Text = reader.GetString(5);
                            lbl24.Text = reader.GetString(6);

                        }
                    }

                    string Thief = "SELECT * FROM CharacterTable";

                    using (SqlCommand command = new SqlCommand(Thief, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            reader.Read();
                            lbl25.Text = reader.GetString(1);
                            lbl26.Text = reader.GetString(2);
                            lbl27.Text = reader.GetString(3);
                            lbl28.Text = reader.GetString(4);
                            lbl29.Text = reader.GetString(5);
                            lbl30.Text = reader.GetString(6);

                            reader.Read();
                            lbl31.Text = reader.GetString(1);
                            lbl32.Text = reader.GetString(2);
                            lbl33.Text = reader.GetString(3);
                            lbl34.Text = reader.GetString(4);
                            lbl35.Text = reader.GetString(5);
                            lbl36.Text = reader.GetString(6);

                        }
                    }

                    string Warrior = "SELECT * FROM CharacterTable";

                    using (SqlCommand command = new SqlCommand(Warrior, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            reader.Read();
                            lbl37.Text = reader.GetString(1);
                            lbl38.Text = reader.GetString(2);
                            lbl39.Text = reader.GetString(3);
                            lbl40.Text = reader.GetString(4);
                            lbl41.Text = reader.GetString(5);
                            lbl42.Text = reader.GetString(6);

                            reader.Read();
                            lbl43.Text = reader.GetString(1);
                            lbl44.Text = reader.GetString(2);
                            lbl45.Text = reader.GetString(3);
                            lbl46.Text = reader.GetString(4);
                            lbl47.Text = reader.GetString(5);
                            lbl48.Text = reader.GetString(6);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }
    }
}
